'use strict';

var path    = require('path');
var util    = require('util');
var _       = require('lodash');

// Set approot for easy access require paths.
GLOBAL.APPROOT = path.resolve(__dirname);

var bot     = require(APPROOT + '/src/irc/bot');
var server  = require(APPROOT + '/src/server/server');

var config  = require(APPROOT + '/config/config.json');

var webServer = new server(config.server);
var ircBot = new bot(config.irc);

webServer.start();
ircBot.start();

var pushEvent = function(data) {
    var projectName = data.repository.name;
    var pusher = data.user_name + ' <' + data.user_email + '>';
    var branch = data.ref.substr(11);

    ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 pushed \u000307%s\u0003 new commit%s to \u000307%s\u0003',
        projectName,
        pusher,
        data.commits.length,
        data.commits.length === 1 ? '' : 's',
        branch
    ));

    _.forEach(data.commits, function(commit) {
        var string = util.format('\u0002%s\u0002: %s \u001f%s\u000f',
            projectName,
            commit.message.replace(/[\r\n]*/g, ''),
            commit.url
        );
        ircBot.send(string);
    });
};

var tagEvent = function(data) {
    var projectName = data.repository.name;
    var pusher = data.user_name;
    var tag = data.ref.substr(11);

    ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 pushed tag \u000307%s\u0003',
        projectName,
        pusher,
        tag
    ));
};

var issueEvent = function(data) {
    var user = data.user.name;
    var issue = data.object_attributes.iid;
    var url = data.object_attributes.url;
    var action = data.object_attributes.action;
    var title = data.object_attributes.title;
    var description = data.object_attributes.description;
    var projectName = url.split('/')[4];

    if (action === 'update') {
        // Update events are annoying.
        return;
    }

    ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 %s issue \u000307#%s\u0003: %s',
        projectName,
        user,
        action === 'open' ? 'opened' : 'closed',
        issue,
        title
    ));

    if (action === 'open') {
        ircBot.send(util.format('\u0002%s\u0002: %s',
            projectName,
            description
        ));
    }

    ircBot.send(util.format('\u0002%s\u0002: \u001f%s\u000f',
        projectName,
        url
    ));
};

var commitCommentEvent = function(data) {
    var user = data.user.name;
    var commit = data.commit.id;
    var message = data.commit.message;
    var projectName = data.repository.name;
    var url = data.object_attributes.url;

    ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 commented on commit \u000307%s\u0003',
        projectName,
        user,
        commit
    ));

    ircBot.send(util.format('\u0002%s\u0002: %s \u001f%s\u000f',
        projectName,
        message,
        url
    ));
};

var mergeRequestCommentEvent = function(data) {
    var user = data.user.name;
    var mergeRequest = data.merge_request.iid;
    var message = data.object_attributes.note;
    var projectName = data.repository.name;
    var url = data.object_attributes.url;

    ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 commented on merge request \u000307!%s\u0003',
        projectName,
        user,
        mergeRequest
    ));

    ircBot.send(util.format('\u0002%s\u0002: %s \u001f%s\u000f',
        projectName,
        message,
        url
    ));
};

var issueCommentEvent = function(data) {
    var user = data.user.name;
    var issue = data.issue.iid;
    var message = data.object_attributes.note;
    var projectName = data.repository.name;
    var url = data.object_attributes.url;

    ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 commented on issue \u000307#%s\u0003',
        projectName,
        user,
        issue
    ));

    ircBot.send(util.format('\u0002%s\u0002: %s \u001f%s\u000f',
        projectName,
        message,
        url
    ));
};

var snippetCommentEvent = function(data) {
    var user = data.user.name;
    var snippet = data.snippet.id;
    var message = data.object_attributes.note;
    var projectName = data.repository.name;
    var url = data.object_attributes.url;

    ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 commented on snippet \u000307%s\u0003',
        projectName,
        user,
        snippet
    ));

    ircBot.send(util.format('\u0002%s\u0002: %s \u001f%s\u000f',
        projectName,
        message,
        url
    ));
};

var selectCommentEvent = function(data) {
    switch (data.object_attributes.noteable_type) {
        case 'Commit':
            commitCommentEvent(data);
            break;
        case 'MergeRequest':
            mergeRequestCommentEvent(data);
            break;
        case 'Issue':
            issueCommentEvent(data);
            break;
        case 'Snippet':
            snippetCommentEvent(data);
            break;
        default:
            break;
    }
};

var mergeRequestEvent = function(data) {
    var user = data.user.name;
    var mergeRequest = data.object_attributes.iid;
    var projectName = data.repository.name;
    var url = data.object_attributes.url;
    var branch = data.object_attributes.target_branch;
    var action = data.object_attributes.action;

    if (action === 'open') {
        ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 opened merge request \u000307!%s\u0003 targetting \u000307%s\u0003',
            projectName,
            user,
            mergeRequest,
            branch
        ));

        ircBot.send(util.format('\u0002%s\u0002: \u001f%s\u000f',
            projectName,
            url
        ));
    } else {
        ircBot.send(util.format('\u0002%s\u0002: \u000303%s\u0003 closed merge request \u000307!%s\u0003',
            projectName,
            user,
            mergeRequest
        ));

        ircBot.send(util.format('\u0002%s\u0002: \u001f%s\u000f',
            projectName,
            url
        ));
    }
};

// Wait for IRC Client to be connected to hook webserver events.
ircBot.on('connected', function() {
    webServer.on('hook', function(data) {
        if (typeof data.object_kind !== 'undefined' && data.object_kind !== null) {
            switch (data.object_kind) {
                case 'push':
                    pushEvent(data);
                    break;
                case 'tag_push':
                    tagEvent(data);
                    break;
                case 'issue':
                    issueEvent(data);
                    break;
                case 'note':
                    selectCommentEvent(data);
                    break;
                case 'merge_request':
                    mergeRequestEvent(data);
                    break;
                default:
                    console.error('Unknown object_kind: ' + data.object_kind);
                    console.dir(data.object_kind);
                    break;
            }
        }
    });
});
