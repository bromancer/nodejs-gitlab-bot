'use strict';

var irc     = require('irc');
var events  = require('events');
var _       = require('lodash');

var allowedOptions = [
    'userName',
    'realName',
    'password',
    'port',
    'debug',
    'showErrors',
    'autoRejoin',
    'autoConnect',
    'secure',
    'selfSigned',
    'certExpired',
    'floodProtection',
    'floodProtectionDelay',
    'sasl',
    'stripColors',
    'channelPrefixes',
    'messageSplit'
];

var Bot = function(configuration) {
    this.configuration = configuration;
    this.connected = false;
    this.channels = this.configuration.channels;
    if (!Array.isArray(this.channels)) {
        this.channels = [this.channels];
    }
};

Bot.prototype = new events.EventEmitter;

Bot.prototype.start = function() {
    this.server = this.configuration.server;
    this.nick = this.configuration.nick;
    this.client = new irc.Client(this.server, this.nick, _.pick(this.configuration, allowedOptions));
    this.client.on('registered', function() {
        _.forEach(this.channels, function(channel) {
            this.client.join(channel);
        }.bind(this));
        this.connected = true;
        this.emit('connected');
    }.bind(this));
};

Bot.prototype.send = function(message) {
    _.forEach(this.channels, function(channel) {
        this.client.say(channel, message);
    }.bind(this));
}

module.exports = Bot;
