'use strict';

var http    = require('http');
var events  = require('events');

var Server = function(configuration) {
    this.configuration = configuration;
};

Server.prototype = new events.EventEmitter;

Server.prototype.start = function() {
    var self = this;
    http.createServer(function(req, res) {
        var buffer = "";
        req.on('data', function(data) {
            console.log('receiving web data...');
            buffer += data;
        });
        req.on('end', function() {
            try {
                self.emit('hook', JSON.parse(buffer));
            } catch (e) {
                console.log('Invalid JSON: ' + buffer);
            } finally {
                buffer = "";
                res.end();
            }
        });
        res.writeHead(200, {
            'ContentType': 'text/plain'
        });
    }).listen(this.configuration.port);
};

module.exports = Server;
