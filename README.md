# Gitlab IRC Webhook Reporter
This is a simple IRC bot which reports all webhooks sent by gitlab to the specified channels.

## Installation
Simply check out the repository and then run the following command to install:
```
> npm install
```

## Running
After installing the bot, copy or rename the ```config.example.json``` file to ```config.json``` in the same folder and edit the file to match your configuration.

After than run the following command:
```
> npm start
```

## ZNC
This bot is intended to work with ZNC. It can connect directly to an irc server but it has poor disconnect recovery, it's better to let the ZNC handle it.
